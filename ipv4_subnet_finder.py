import re

from subnet_finder import SubnetFinder


class IPv4SubnetFinder(SubnetFinder):
    def __init__(self):
        super().__init__()
        self._octet_size = 8
        self._octet_max_value = 255

    def get_result_as_string(self):
        subnet = self._minimal_subnet
        result = ""
        for i in range(4):
            octet = subnet & self._octet_max_value
            result = '.' + str(octet) + result
            subnet = subnet >> self._octet_size
        result = result[1:]
        result = f"{result}/{self._bit_len}"
        return result

    def _parse_ip(self, line):
        regex_res = re.match(r"(\d{1,3}\.){3}\d{1,3}", line)
        if regex_res.group(0) and len(regex_res.group(0)) == len(line):
            regex_res = regex_res.group(0)
            ip = regex_res.split('.')
            ip.reverse()
            result = 0
            for shift, octet in enumerate(ip, 0):
                octet = int(octet)
                if octet > self._octet_max_value or octet < 0:
                    err_message = f"Octet ({octet}) in ip {line} not correct"
                    raise Exception(err_message)
                result |= int(octet) << (shift * self._octet_size)
        else:
            raise Exception(f"Ip adress {line} not correct")
        return result
