import re

from subnet_finder import SubnetFinder


class IPv6SubnetFinder(SubnetFinder):
    def __init__(self):
        super().__init__()
        self._hextet_size = 16
        self._hextets_count = 8

    def get_result_as_string(self):
        subnet_as_list = self._subnet_as_list()
        max_zeros_sequence = self._find_max_zeros_sequence(subnet_as_list)

        result = ""
        for i, hextet in enumerate(subnet_as_list):
            # Ставим :: вместо последовательности нулей.
            if i in max_zeros_sequence:
                if i != max_zeros_sequence[0]:
                    continue
                if result:
                    if max_zeros_sequence[-1] == len(subnet_as_list) - 1:
                        result += "::"
                    else:
                        result += ":"
                else:
                    result = "::"
            else:
                result += f":{hex(hextet)[2:]}"
        result = result[1:]
        result = f"{result}/{self._bit_len}"

        return result

    def _subnet_as_list(self):
        max_hextet = 0
        for i in range(self._hextet_size):
            max_hextet |= 2 ** i

        subnet = self._minimal_subnet
        subnet_as_list = []
        for i in range(self._hextets_count):
            hextet = subnet & max_hextet
            subnet_as_list.append(hextet)
            subnet = subnet >> self._hextet_size
        subnet_as_list.reverse()
        return subnet_as_list

    def _find_max_zeros_sequence(self, subnet_as_list):
        zeros_indexes = [[]]
        for i, hextet in enumerate(subnet_as_list):
            if hextet == 0:
                zeros_indexes[-1].append(i)
            else:
                if zeros_indexes[-1]:
                    zeros_indexes.append([])

        max_zeros_sequence = None
        max_zeros_len = 0
        for zeros_sequence in zeros_indexes:
            zeros_len = len(zeros_sequence)
            if max_zeros_sequence is None or zeros_len > max_zeros_len:
                max_zeros_sequence = zeros_sequence
                max_zeros_len = zeros_len

        return max_zeros_sequence


    def _parse_ip(self, line):
        regex_res = re.match(r"[:\da-fA-F]+", line)
        if regex_res.group(0) != line:
            err_descr = "Ip address includes improper symbols"
            raise Exception(self._error_message_for_parser(line, err_descr))
        null_count = len(re.findall(r'::', line))
        if null_count > 1:
            err_descr = "Too many '::' pattern"
            raise Exception(self._error_message_for_parser(line, err_descr))
        hextets = line.split(':')
        if len(hextets) > 8:
            err_descr = "Too many hextets"
            raise Exception(self._error_message_for_parser(line, err_descr))
        hextets.reverse()
        result = 0
        shift = 0
        for hextet in hextets:
            if hextet:
                hextet = int(hextet, 16)
                result |= hextet << self._hextet_size * shift
                shift += 1
            else:  # вставляем нули вместо ::
                # прибавляем 1 к количеству т. к. 1 группа нулей уже заложена
                # в список хекстетов как пустая строка
                insert_count = self._hextets_count - len(hextets) + 1
                # прибаляем смещение, т. к. идем по наборам в ip справа налево
                shift += insert_count
        return result

    def _error_message_for_parser(self, ip, description):
        return f"Invalid ip '{ip}' for IPv6 paser. {description}"
