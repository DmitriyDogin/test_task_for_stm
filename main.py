import argparse
import sys

from ipv4_subnet_finder import IPv4SubnetFinder
from ipv6_subnet_finder import IPv6SubnetFinder


def create_subnet_finder(version):
    version = version.lower()
    if version == "ipv4":
        subnet_finder = IPv4SubnetFinder()
    elif version == "ipv6":
        subnet_finder = IPv6SubnetFinder()
    else:
        raise Exception("Wrong ip version, select IPv4 or IPv6")
    return subnet_finder


def find_minimal_subnet(ip_version, filename):
    subnet_finder = create_subnet_finder(ip_version)
    subnet_finder.read_adresses_from_file(filename)
    subnet_finder.find_minimal_subnet()
    return subnet_finder.get_result_as_string()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--file")
    parser.add_argument("--ip_version")
    args = parser.parse_args(sys.argv[1:])
    minimal_subnet = find_minimal_subnet(args.ip_version, args.file)
    print(f"Result net: {minimal_subnet}")


if __name__ == "__main__":
    main()
