import typing
from typing import List


class SubnetFinder:
    def __init__(self):
        self._ip_addresses: List[int] = []
        self._minimal_subnet: typing.Optional[int] = None
        self._bit_len: typing.Optional[int] = None

    def read_adresses_from_file(self, filename):
        with open(filename, 'r') as file:
            for line in file:
                line = line.strip()
                ip = self._parse_ip(line)
                self._ip_addresses.append(ip)
        if not self._ip_addresses:
            raise Exception("file not exists or empty")

    def find_minimal_subnet(self):
        least_mask = None
        for i in range(0, len(self._ip_addresses)):
            if i + 1 == len(self._ip_addresses):
                mask, bit_len = self.find_mask(self._ip_addresses[i - 1],
                                               self._ip_addresses[i])
            else:
                mask, bit_len = self.find_mask(self._ip_addresses[i],
                                               self._ip_addresses[i + 1])

            if least_mask is None or mask < least_mask:
                least_mask = mask
                self._bit_len = bit_len
                if bit_len == 0:  # маску < 0 не найти, можем заканчивать
                    break
        self._minimal_subnet = least_mask & self._ip_addresses[0]

    def get_result_as_string(self):
        raise Exception("Need to rewrite method get_result_as_string")

    @staticmethod
    def find_mask(ip1, ip2):
        number = ip1 ^ ip2
        number_bit_len = 0
        # Определяем длинну битовой последовательности числа, полученного
        # после операции 'исключающего или'.
        while number:
            number_bit_len += 1
            number = number >> 1
        # Считаем разницу между изначальной длинной и длиной number
        equal_position = 0
        ip1 = ip1 >> number_bit_len
        while ip1:
            ip1 = ip1 >> 1
            equal_position += 1
        mask = 0
        # Результат - набор едениц, смещенных влево на размер битовой
        # последовательности после операции исключающего или
        for i in range(equal_position):
            mask |= 2 ** i
        mask = mask << number_bit_len
        return mask, equal_position

    def _parse_ip(self, line):
        raise Exception("Need to rewrite method parse_ip")
