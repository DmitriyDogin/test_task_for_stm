import pytest

from ipv4_subnet_finder import IPv4SubnetFinder


def test_get_result_as_string():
    subnet_finder = IPv4SubnetFinder()

    subnet_finder._minimal_subnet = 0b11111111111111111111111111111111
    subnet_finder._bit_len = 0
    assert subnet_finder.get_result_as_string() == "255.255.255.255/0"

    subnet_finder._bit_len = 4
    subnet_finder._minimal_subnet = 0b11000000101010000000000100000000
    assert subnet_finder.get_result_as_string() == "192.168.1.0/4"


def test_parse_ip():
    subnet_finder = IPv4SubnetFinder()

    assert subnet_finder._parse_ip("255.255.255.255") == \
           0b11111111111111111111111111111111

    assert subnet_finder._parse_ip("255.255.255.0") == \
           0b11111111111111111111111100000000

    assert subnet_finder._parse_ip("192.168.1.0") == \
           0b11000000101010000000000100000000

    with pytest.raises(Exception):
        subnet_finder._parse_ip("192,168,1,1")

    with pytest.raises(Exception):
        subnet_finder._parse_ip("2555.255.192.0")

    with pytest.raises(Exception):
        subnet_finder._parse_ip("256.255.192.0")
