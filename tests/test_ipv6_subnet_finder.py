import pytest

from ipv6_subnet_finder import IPv6SubnetFinder


def test_get_result_as_string():
    subnet_finder = IPv6SubnetFinder()

    subnet_finder._bit_len = 0
    subnet_finder._minimal_subnet = 1
    assert subnet_finder.get_result_as_string() == "::1/0"

    subnet_finder._bit_len = 1
    subnet_finder._minimal_subnet = 83093480812927417407890734574338109
    assert subnet_finder.get_result_as_string() == \
           "10:d3:2d06:24:400c:5ee0:be:3d/1"

    subnet_finder._bit_len = 1
    subnet_finder._minimal_subnet = 41700635795292555857958452489879552
    assert subnet_finder.get_result_as_string() == "8:800:555:35:35::/1"

    subnet_finder._bit_len = 29
    subnet_finder._minimal_subnet = 42540765777457378815297132132070064176
    assert subnet_finder.get_result_as_string() == "2001:db0:0:123a::30/29"


def test_parse_ip():
    subnet_finder = IPv6SubnetFinder()

    # 00000000000100000000000011010011001011010000011000000000001001000100000000001100010111101110000000000000101111100000000000111101
    assert subnet_finder._parse_ip("10:d3:2d06:24:400c:5ee0:be:3d") == \
           83093480812927417407890734574338109

    # 00100000000000010000110110110000000000000000000000010010001110100000000000000000000000000000000000000000000000000000000000110000
    ip_address = "2001:0DB0:0000:123A:0000:0000:0000:0030"
    assert subnet_finder._parse_ip(ip_address) == \
           42540765777457378815297132132070064176

    assert subnet_finder._parse_ip("2001:DB0:0:123A::30") == \
           42540765777457378815297132132070064176

    # 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001
    assert subnet_finder._parse_ip("::1") == 1

    with pytest.raises(Exception):
        subnet_finder._parse_ip("::ff::12")

    with pytest.raises(Exception):
        subnet_finder._parse_ip("12:a4:1:345:ff:4a:bc:30:3113")

    with pytest.raises(Exception):
        subnet_finder._parse_ip("12.1.3.45")
