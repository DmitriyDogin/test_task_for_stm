import os
import pytest

from main import create_subnet_finder, find_minimal_subnet
from ipv4_subnet_finder import IPv4SubnetFinder
from ipv6_subnet_finder import IPv6SubnetFinder


def test_create_subnet_finder():
    finder = create_subnet_finder("ipv4")
    assert isinstance(finder, IPv4SubnetFinder)

    finder = create_subnet_finder("IPv6")
    assert isinstance(finder, IPv6SubnetFinder)

    with pytest.raises(Exception):
        create_subnet_finder("ipv0")


def test_find_minimal_subnet():
    if(os.path.exists(os.path.join(os.curdir, "tests"))):
        os.chdir("tests")

    assert find_minimal_subnet("ipv4", "data/ipv4.txt") == "192.168.1.0/29"
    assert find_minimal_subnet("ipv6", "data/ipv6.txt") == "ffe0::/72"
