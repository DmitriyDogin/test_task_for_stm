from subnet_finder import SubnetFinder


def test_find_minimal_subnet():
    subnet_finder = SubnetFinder()
    subnet_finder._ip_addresses = [3232235778, 3232235779, 3232235781]
    subnet_finder.find_minimal_subnet()
    assert subnet_finder._minimal_subnet == 3232235776
    assert subnet_finder._bit_len == 29


def test_find_mask():
    assert SubnetFinder.find_mask(209, 216) == (240, 4)
    assert SubnetFinder.find_mask(465, 216) == (0, 0)
    assert SubnetFinder.find_mask(39340, 39852) == (64512, 6)
